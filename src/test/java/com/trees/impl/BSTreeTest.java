package com.trees.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.lang.reflect.Field;

class BSTreeTest {

    BSTree cut = new BSTree();

    static Arguments[] addTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT1), 0, 47, BSTreeTestData.addTreeTest1),
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT2), 8, 55, BSTreeTestData.addTreeTest2)
        };
    }

    @ParameterizedTest
    @MethodSource("addTestArgs")
    void addTest(Node reflect, int reflectSize, int value, BSTree expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        cut.add(value);

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] delTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT2), 8, 30, BSTreeTestData.deleteTreeTest1, true),
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT2), 8, 150, BSTreeTestData.deleteTreeTest2, true)
        };
    }

    @ParameterizedTest
    @MethodSource("delTestArgs")
    void delTest(Node reflect, int reflectSize, int value, BSTree expected, boolean boolExpected)
            throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        boolean actual = cut.del(value);

        Assertions.assertEquals(expected, cut);
        Assertions.assertEquals(boolExpected, actual);
    }

    static Arguments[] clearTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT1), 0, BSTreeTestData.originalTreeTest1),
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT2), 8, BSTreeTestData.originalTreeTest1)
        };
    }

    @ParameterizedTest
    @MethodSource("clearTestArgs")
    void clearTest(Node reflect, int reflectSize, BSTree expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        cut.clear();

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] sizeTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT1), 0, 23, 1),
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT2), 8, 45, 9)
        };
    }

    @ParameterizedTest
    @MethodSource("sizeTestArgs")
    void sizeTest(Node reflect, int reflectSize, int numAdd, int expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        cut.add(numAdd);
        int actual = cut.size();
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] toArrayTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT1), 0, new int[] {}),
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT2), 8, new int[] {100, 150, 50, 200, 30, 250, 175, 40})
        };
    }

    @ParameterizedTest
    @MethodSource("toArrayTestArgs")
    void toArrayTest(Node reflect, int reflectSize, int[] expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        int[] actual = cut.toArray();

        Assertions.assertArrayEquals(expected, actual);
    }

    static Arguments[] printTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT1), 0, "[]"),
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT2), 8, "[30, 40, 50, 100, 150, 175, 200, 250]")
        };
    }

    @ParameterizedTest
    @MethodSource("printTestArgs")
    void printTest(Node reflect, int reflectSize, String expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        //тест на toString() бо print() лише цей метод викликає
        String actual = cut.toString();

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] initTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT_REVERSE1), 0, new int[] {}, BSTreeTestData.originalTreeTest1),
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT_REVERSE1), 8, new int[] {100, 50, 150, 30, 200, 40, 175, 250}, BSTreeTestData.originalTreeTest2),
        };
    }

    @ParameterizedTest
    @MethodSource("initTestArgs")
    void initTest(Node reflect, int reflectSize, int[] array, BSTree expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        cut.init(array);

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] nodesTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT1), 0, 0),
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT2), 8, 5)
        };
    }

    @ParameterizedTest
    @MethodSource("nodesTestArgs")
    void nodesTest(Node reflect, int reflectSize, int expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        int actual = cut.nodes();
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] leavesTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT1), 0, 0),
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT2), 8, 3)
        };
    }

    @ParameterizedTest
    @MethodSource("leavesTestArgs")
    void leavesTest(Node reflect, int reflectSize, int expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        int actual = cut.leaves();
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getWidthTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT1), 0, 0),
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT2), 8, 3)
        };
    }

    @ParameterizedTest
    @MethodSource("getWidthTestArgs")
    void getWidthTest(Node reflect, int reflectSize,  int expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        int actual = cut.getWidth();
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getHeightTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT1), 0, 0),
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT2), 8, 4)
        };
    }

    @ParameterizedTest
    @MethodSource("getHeightTestArgs")
    void getHeightTest(Node reflect, int reflectSize,  int expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        int actual = cut.getHeight();
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] reverseTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT1), 0, BSTreeTestData.reverseTreeTest1),
                Arguments.arguments(Node.deepCopy(BSTreeTestData.ROOT2), 8, BSTreeTestData.reverseTreeTest2)
        };
    }

    @ParameterizedTest
    @MethodSource("reverseTestArgs")
    void reverseTest(Node reflect, int reflectSize,  BSTree expected)
            throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        cut.reverse();

        Assertions.assertEquals(expected, cut);
    }
}