package com.trees.interfaces;

public interface ITree {

    void init(int[] arr);

    void print();

    void clear();

    int size();

    int[] toArray();

    void add(int val);

    boolean del(int val);

    int getWidth();

    int getHeight();

    int nodes();

    int leaves();

    void reverse();

}
