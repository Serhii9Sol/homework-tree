package com.trees.impl;

import com.trees.interfaces.ITree;
import java.util.*;

public class BSTree implements ITree {

    private int size = 0;
    private Node root;

    @Override
    public void init(int[] arr) {
        clear();
        for (int j : arr) {
            add(j);
        }
    }

    @Override
    public void print() {
        System.out.println(toString());
    }

    @Override
    public void clear() {
        root = null;
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int[] toArray() {
        if (size == 0) {
            return new int[0];
        }
        int[] array = new int[size];
        ArrayList<Node> nodeArrayList = new ArrayList<>();
        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            Node node = queue.poll();
            nodeArrayList.add(node);

            if (node.right != null) {
                queue.add(node.right);
            }

            if (node.left != null) {
                queue.add(node.left);
            }
        }
        Iterator<Node> iterator = nodeArrayList.iterator();
        for (int i = 0; i < array.length; i++) {
            Node current = iterator.next();
            array[i] = current.item;
        }
        return array;
    }

    @Override
    public void add(int val) {
        Node newNode = new Node(val);
        if (root == null) {
            root = newNode;
        } else {
            Node currentNode = root;
            while (true) {
                int nodeVal = currentNode.item;
                if (nodeVal == val) {
                    return;
                } else if (val < nodeVal) {
                    if (currentNode.left == null) {
                        currentNode.left = newNode;
                        break;
                    } else {
                        currentNode = currentNode.left;
                    }
                } else {
                    if (currentNode.right == null) {
                        currentNode.right = newNode;
                        break;
                    } else {
                        currentNode = currentNode.right;
                    }
                }
            }
        }
        size++;
    }

    @Override
    public boolean del(int val) {
        if (root == null) {
            return false;
        }
        Node currentNode = root;
        Node parentNode = null;
        boolean isLeftChild = true;
        while (currentNode.item != val) {
            parentNode = currentNode;
            if (val < currentNode.item) {
                isLeftChild = true;
                currentNode = currentNode.left;
            } else {
                isLeftChild = false;
                currentNode = currentNode.right;
            }
            if (currentNode == null) {
                return false;
            }
        }

        if (currentNode.left == null && currentNode.right == null) {
            if (currentNode == root) {
                root = null;
            } else if (isLeftChild) {
                parentNode.left = null;
            } else {
                parentNode.right = null;
            }
        } else if (currentNode.right == null) {
            if (currentNode == root) {
                root = currentNode.left;
            } else if (isLeftChild) {
                parentNode.left = currentNode.left;
            } else {
                parentNode.right = currentNode.left;
            }
        } else if (currentNode.left == null) {
            if (currentNode == root) {
                root = currentNode.right;
            } else if (isLeftChild) {
                parentNode.left = currentNode.right;
            } else {
                parentNode.right = currentNode.right;
            }
        } else {
            Node heir = receiveHeir(currentNode);
            if (currentNode == root) {
                root = heir;
            } else if (isLeftChild) {
                parentNode.left = heir;
            } else {
                parentNode.right = heir;
            }
        }
        size--;
        return true;
    }

    @Override
    public int getWidth() {
        if (root == null) {
            return 0;
        }
        int maxwidth = 0;
        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            int count = queue.size();
            maxwidth = Math.max(maxwidth, count);
            while (count > 0) {
                Node current = queue.poll();

                if (current.left != null) {
                    queue.add(current.left);
                }
                if (current.right != null) {
                    queue.add(current.right);
                }
                count--;
            }
        }
        return maxwidth;
    }

    @Override
    public int getHeight() {
        if (root == null) {
            return 0;
        }
        Queue<Node> queue = new ArrayDeque<>();
        queue.add(root);
        Node current = null;
        int height = 0;
        while (!queue.isEmpty()) {
            int count = queue.size();
            while (count > 0) {
                current = queue.poll();
                if (current.left != null) {
                    queue.add(current.left);
                }
                if (current.right != null) {
                    queue.add(current.right);
                }
                count--;
            }
            height++;
        }
        return height;
    }

    @Override
    public int nodes() {
        if (root == null) {
            return 0;
        }
        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        int nodesNotLeaves = 0;
        while (!queue.isEmpty()) {
            int count = queue.size();
            while (count > 0) {
                Node current = queue.poll();
                if (current.left != null) {
                    queue.add(current.left);
                }
                if (current.right != null) {
                    queue.add(current.right);
                }
                if (current.left != null || current.right != null) {
                    nodesNotLeaves++;
                }
                count--;
            }
        }
        return nodesNotLeaves;
    }

    @Override
    public int leaves() {
        return size - nodes();
    }

    @Override
    public void reverse() {
        Queue<Node> queue = new LinkedList<>();
        if (root != null) {
            queue.add(root);
        }
        while (!queue.isEmpty()) {
            Node node = queue.poll();
            if (node.left != null) {
                queue.add(node.left);
            }
            if (node.right != null) {
                queue.add(node.right);
            }
            if (node.left != null || node.right != null) {
                Node temp = node.left;
                node.left = node.right;
                node.right = temp;
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BSTree bsTree = (BSTree) o;
        return size == bsTree.size && Objects.equals(root, bsTree.root);
    }

    @Override
    public int hashCode() {
        return Objects.hash(size, root);
    }

    @Override
    public String toString() {
        String stringArray = "[";
        int[] array = toArray();
        Arrays.sort(array);
        for (int i = 0; i < array.length; i++) {
            stringArray += array[i];
            if (i != array.length - 1) {
                stringArray += ", ";
            }
        }
        stringArray += "]";
        return stringArray;
    }

    private Node receiveHeir(Node node) {
        Node parentNode = node;
        Node heirNode = node;
        Node currentNode = node.right;
        while (currentNode != null) {
            parentNode = heirNode;
            heirNode = currentNode;
            currentNode = currentNode.left;
        }
        if (heirNode != node.right) {
            parentNode.left = heirNode.right;
            heirNode.right = node.right;
        }
        return heirNode;
    }
}
